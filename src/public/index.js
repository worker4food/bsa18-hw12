import Fighter from './fighter';
import ImprovedFighter from './improvedFighter';
import { fight, fightToVictory } from './fight';

// create two instances
let fighter = new Fighter('Sneaky Novice', 1, 81);
let improvedFighter = new ImprovedFighter('Mature Veteran', 2, 100);

// call fight function
fight(fighter, improvedFighter, 15, 1, 25, 1);
// or
// fightToVictory(fighter, improvedFighter, 15, 1, 25, 1);
