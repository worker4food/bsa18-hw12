// create class Fighter

export default class Fighter {
    constructor(name, power = 1, health = 50) {
        this.name = name;
        this.power = power;
        this.health = health;
    }

    setDamage(damage) {
        this.health -= damage;
        console.log(`${this.name} health: ${this.health}`);
    }

  hit(enemy, point) {
        let hp = point * this.power;
        enemy.setDamage(hp);
        return hp;
  }

    knockout() {
        return new Promise((resolve, _) =>
            setTimeout(() => {
                console.log("time is out");
                resolve();
            }, 500));
    }
}