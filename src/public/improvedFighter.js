// Create class ImprovedFighter

import Fighter from './fighter';

export default class ImprovedFighter extends Fighter{

    doubleHit(enemy, point) {
        return super.hit(enemy, point * 2);
    }

    hit(enemy, point) {
        return this.doubleHit(enemy, point);
    }
}