// create async function fight

import wu from 'wu'

let fightImp = async (fstF, sndF, points) =>
    await wu.zip(wu.cycle([fstF, sndF]), wu.cycle([sndF, fstF]), points)
        .tap(([f1, f2, p]) => f1.hit(f2, p))
        .dropWhile(([_, x]) => x.health > 0)
        .map(([_, x]) => x.knockout()
            .then(() => console.log(`${x.name} knocked out!`)))
        .next()
        .value;

export function fight(fstFighter, sndFighter, ...points) {
    return fightImp(fstFighter, sndFighter, points);
}

export function fightToVictory(fstFighter, sndFighter, ...pattern) {
    return fightImp(fstFighter, sndFighter, wu.cycle(pattern));
}
